16)    SELECT DISTINCT PC1.model, PC2.model, PC1.speed, PC1.ram FROM PC PC1 JOIN PC PC2 ON PC1.speed = PC2.speed AND PC1.ram = PC2.ram AND PC1.model > PC2.model;
17)    SELECT DISTINCT type, Laptop.model, speed FROM Laptop, Product WHERE speed < ALL(SELECT speed FROM PC) AND Laptop.model = Product.model;
18)    SELECT DISTINCT maker, price FROM Product JOIN Printer ON Product.model = Printer.model AND Printer.price = (SELECT min(price) From Printer WHERE Printer.color = 'y') AND Printer.color = 'y';
19)    SELECT maker, AVG(screen) FROM Product JOIN Laptop ON Product.model = Laptop.model GROUP BY maker;
20)    SELECT maker, COUNT(model) FROM Product WHERE type = 'PC' GROUP BY maker HAVING COUNT(Product.model) >= 3;
21)    SELECT maker, MAX(price) FROM Product JOIN PC ON Product.model = PC.model GROUP BY maker;
22)    SELECT speed, AVG(price) FROM PC GROUP BY speed HAVING speed > 600;
23)    SELECT DISTINCT maker FROM Product JOIN PC ON Product.model = PC.model AND speed >= 750
       INTERSECT
       SELECT DISTINCT maker FROM Product JOIN Laptop ON Product.model = Laptop.model AND speed >= 750;
24)    WITH All_Prod AS (SELECT price, model FROM PC UNION ALL SELECT price, model FROM Laptop UNION ALL SELECT price, model FROM Printer)
       SELECT DISTINCT model FROM All_Prod WHERE price = (SELECT MAX(price) FROM All_Prod);
25)    SELECT DISTINCT maker FROM Product JOIN PC ON Product.model = PC.model WHERE maker IN (SELECT DISTINCT maker FROM Product WHERE type = 'Printer') AND ram = (SELECT MIN(ram) FROM PC) AND speed = (SELECT MAX(speed) FROM PC WHERE ram = (SELECT MIN(ram) FROM PC));
26)    WITH PC_LP AS (SELECT price FROM Product JOIN PC ON PC.model = Product.model AND maker = 'A' UNION ALL SELECT price FROM Product JOIN Laptop ON Laptop.model = Product.model AND maker = 'A') SELECT AVG(price) FROM PC_LP;
27)    SELECT maker, AVG(hd) FROM Product JOIN PC ON PC.model = Product.model AND maker IN (SELECT maker FROM Product WHERE type = 'Printer') GROUP BY maker;

