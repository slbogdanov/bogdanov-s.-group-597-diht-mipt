USE master
GO

/* Creating Database */

IF EXISTS(SELECT * from sys.databases WHERE name='TestDB')
BEGIN
    DROP DATABASE TestDB
END

CREATE DATABASE TestDB
GO

USE TestDB
GO

IF EXISTS(SELECT * from sys.tables WHERE name='Bank')
BEGIN
DROP TABLE Bank
END
GO
IF EXISTS(SELECT * from sys.tables WHERE name='Robber')
BEGIN
DROP TABLE Robber
END
GO
IF EXISTS(SELECT * from sys.tables WHERE name='BankRobber')
BEGIN
DROP TABLE BankRobber
END
GO
IF EXISTS(SELECT * from sys.tables WHERE name='Contacts')
BEGIN
DROP TABLE Contacts
END
GO

CREATE TABLE [dbo].[Bank](
	[Address] [varchar](100) NOT NULL,
	[SecurityLevel] [int] NOT NULL,
	[BankID] [int] IDENTITY(1,1) NOT NULL,
	[BankName] [varchar](100) NOT NULL,
	[DailyIncome] [int] NOT NULL,
	[Attractiveness] [int] NOT NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY
(
	[BankID] ASC
)
);

CREATE TABLE [dbo].[BankRobber](
	[RobberID] [int] NOT NULL,
	[BankID] [int] NOT NULL,
	[Share] [int] NOT NULL,
	[OutcomeForRobber] [varchar](100) NOT NULL,
	[ActionsEvaluation] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_BankRobber] PRIMARY KEY
(
	[RobberID] ASC,
	[BankID] ASC
)
);

CREATE TABLE [dbo].[Robber](
	[Status] [varchar](100) NOT NULL,
	[SkillLevel] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Nickname] [varchar](100) NOT NULL,
	[RobberID] [int] NOT NULL,
	[Spec] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Robber] PRIMARY KEY 
(
	[RobberID] ASC
)
);

CREATE TABLE [dbo].[Contacts](
	[RobberID] [int] IDENTITY(1,1) NOT NULL,
	[Contact] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY 
(
	[RobberID] ASC
)
);



ALTER TABLE [dbo].[BankRobber]  WITH CHECK ADD CONSTRAINT [FK_BankRobber_Bank] FOREIGN KEY([BankID])
REFERENCES [dbo].[Bank] ([BankID])

ALTER TABLE [dbo].[BankRobber]  WITH CHECK ADD  CONSTRAINT [FK_BankRobber_Robber] FOREIGN KEY([RobberID])
REFERENCES [dbo].[Robber] ([RobberID])

ALTER TABLE [dbo].[Robber]  WITH CHECK ADD  CONSTRAINT [FK_Robber_Contacts] FOREIGN KEY([RobberID])
REFERENCES [dbo].[Contacts] ([RobberID])

ALTER TABLE [Bank]
ADD CONSTRAINT [CK_Bank_Attractiveness]
CHECK ([Attractiveness] > 0 AND [Attractiveness] < 11);

ALTER TABLE [Bank]
ADD CONSTRAINT [CK_Bank_SecurityLevel]
CHECK ([SecurityLevel] > 0 AND [SecurityLevel] < 11);

ALTER TABLE [Robber]
ADD CONSTRAINT [CK_Robber_SkillLevel]
CHECK ([SkillLevel] > 0 AND [SkillLevel] < 11);


SET IDENTITY_INSERT [Bank] ON
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('somewhere1', 1, 1, 'moneybags2', 9000, 9)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('somewhere2', 3, 2, 'moneybags3', 2000, 1)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('somewhere3', 4, 3, 'moneybags322', 12200, 7)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('whoknowswhere', 5, 4, 'alotofmoney', 1400, 1)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('prettymuchnowhere', 5, 5, 'nomoney', 10040, 6)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('defenetelysomwhere', 2, 6, 'nohoney', 1530, 5)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('aroundhere', 1, 7, 'thirtypiecesofsilver', 1300, 4)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('surelynothere', 3, 8, 'dirtymoney', 15600, 8)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('maybethere', 1, 9, 'legalmoney', 12300, 10)
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('somewhere4', 2, 10, 'madison', 10000, 8)
SET IDENTITY_INSERT [Bank] OFF

SET IDENTITY_INSERT [Contacts] ON
INSERT [Contacts] ([RobberID], [Contact]) VALUES (1, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (2, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (3, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (4, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (5, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (6, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (7, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (8, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (9, 'phone ***********')
INSERT [Contacts] ([RobberID], [Contact]) VALUES (10, 'phone *********** call me maybe')
SET IDENTITY_INSERT [Contacts] OFF

INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('ready', 2, convert(datetime, '2002-08-04', 102), 'Bob', 1, 'hacker')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('ready', 2, convert(datetime, '2002-08-04', 102), 'Rob', 2, 'commando')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('caught', 3, convert(datetime, '2002-05-31', 102), 'Ray', 3, 'psycho')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('ready', 6, convert(datetime, '2002-08-04', 102), 'Jay', 4, 'slacker')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('ready', 2, convert(datetime, '2002-08-04', 102), 'Kay', 5, 'burglar')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('caught', 8, convert(datetime, '2005-06-12', 102), 'Mei', 6, 'ninja')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('ready', 2, convert(datetime, '2002-08-04', 102), 'Tod', 7, 'assault')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('ready', 7, convert(datetime, '2002-08-04', 102), 'Zac', 8, 'sniper')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('dead', 9, convert(datetime, '1990-08-04', 102), 'Kim', 9, 'driver')
INSERT [Robber] ([Status], [SkillLevel], [Date], [Nickname], [RobberID], [Spec]) VALUES ('ready', 10, convert(datetime, '2002-08-04', 102), 'Ash', 10, 'idiot')

INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (1, 1, 'caught', 200, 5, convert(datetime, '2001-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (2, 1, 'caught', 300, 4, convert(datetime, '2001-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (3, 2, 'caught', 400, 2, convert(datetime, '2002-05-31', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (4, 3, 'escaped', 400, 1, convert(datetime, '2000-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (5, 5, 'escaped', 600, 4, convert(datetime, '1999-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (5, 6, 'caught', 500, 7, convert(datetime, '2004-02-04', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (4, 2, 'caught', 200, 5, convert(datetime, '2007-03-09', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (7, 6, 'escaped', 300, 6, convert(datetime, '2008-10-08', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (10, 2, 'caught', 100, 3, convert(datetime, '2001-09-11', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (6, 10, 'escaped', 500, 7, convert(datetime, '2003-11-21', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (6, 9, 'caught', 600, 3, convert(datetime, '2005-06-12', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (1, 8, 'escaped', 200, 1, convert(datetime, '2003-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (2, 7, 'escaped', 200, 9, convert(datetime, '2000-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (3, 7, 'caught', 300, 3, convert(datetime, '2000-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (4, 7, 'escaped', 200, 10, convert(datetime, '2000-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (5, 7, 'escaped', 800, 5, convert(datetime, '2000-01-01', 102))
INSERT [BankRobber] ([RobberID], [BankID], [OutcomeForRobber], [Share], [ActionsEvaluation], [Date]) VALUES (8, 8, 'escaped', 1000, 2, convert(datetime, '2002-08-04', 102))

/* Requests listed in variant */

SELECT Date, stolen, losses FROM
(SELECT K1.Date AS Date, stolen, losses FROM 
(SELECT stolen, Date FROM (SELECT SUM(Share) AS stolen, [Date] FROM [BankRobber] GROUP BY Date) X WHERE stolen = (SELECT MAX(stolen) FROM (SELECT SUM(Share) AS stolen, [Date] FROM [BankRobber] GROUP BY Date) Y))K1
JOIN
(SELECT COUNT(OutcomeForRobber) AS losses, Date FROM [BankRobber] WHERE OutcomeForRobber = 'caught' GROUP BY Date)K2
ON K1.Date = K2.Date)Z 
WHERE losses = 
(SELECT MIN(losses) FROM 
(SELECT K1.Date AS Date, stolen, losses FROM 
(SELECT stolen, Date FROM (SELECT SUM(Share) AS stolen, [Date] FROM [BankRobber] GROUP BY Date) X WHERE stolen = (SELECT MAX(stolen) FROM (SELECT SUM(Share) AS stolen, [Date] FROM [BankRobber] GROUP BY Date) Y))K1
JOIN
(SELECT COUNT(OutcomeForRobber) AS losses, Date FROM [BankRobber] WHERE OutcomeForRobber = 'caught' GROUP BY Date)K2
ON K1.Date = K2.Date)Z )

SELECT DISTINCT Nickname, COUNT(Date) AS count FROM [BankRobber] JOIN
(SELECT [Robber].RobberID, Nickname FROM [Robber] JOIN [BankRobber] ON [Robber].[RobberID] = [BankRobber].RobberID JOIN [Bank] ON [BankRobber].[BankID] = [Bank].[BankID] WHERE [Bank].[BankName] = 'madison' AND [BankRobber].[Date] = convert(datetime, '2003-11-21', 102))X
ON [BankRobber].[RobberID] = X.RobberID
GROUP BY Nickname

SELECT Nickname, Spec, SkillLevel, cnt FROM
(SELECT rank() OVER(ORDER BY Nickname, Spec, SkillLevel) AS cnt, Nickname, Spec, SkillLevel FROM [Robber] R1 WHERE SkillLevel = (SELECT MAX(SkillLevel) FROM [Robber] WHERE Spec = R1.Spec)) K
WHERE cnt < 11

SELECT COUNT(DISTINCT Date) as SuccessfulRobberies, MONTH(Date) as Month  FROM [BankRobber] BR1 WHERE BR1.Date IN (SELECT DISTINCT  Date FROM [BankRobber] WHERE OutcomeForRobber = 'escaped') GROUP BY MONTH(Date)
GO

/* My own requests */

SELECT Nickname AS BestHacker FROM [Robber] WHERE Spec = 'hacker' AND SkillLevel = (SELECT MAX(SkillLevel) FROM [Robber] WHERE Spec = 'hacker')

SELECT stolen AS MaxStolen, Date FROM (SELECT SUM(Share) AS stolen, [Date] FROM [BankRobber] GROUP BY Date) X WHERE stolen = (SELECT MAX(stolen) FROM (SELECT SUM(Share) AS stolen, [Date] FROM [BankRobber] GROUP BY Date)Y)

SELECT BankName AS MostSecureBankInSomewhere1 FROM [Bank] WHERE Address = 'somewhere1' AND SecurityLevel = (SELECT MAX(SecurityLevel) FROM [Bank] WHERE Address = 'somewhere1')

/* My modifications 

UPDATE [Bank] SET
    BankName = SUBSTRING(BankName, 1, 2)

DELETE FROM [BankRobber] WHERE OutcomeForRobber = 'caught'

UPDATE [Bank] SET
    SecurityLevel = SecurityLevel - 1 WHERE Address = 'whoknowswhere'

SELECT BankName, SecurityLevel FROM [Bank]

SELECT RobberID, BankID, OutcomeForRobber FROM [BankRobber]  */



/* Following modifications will not work due to restrictions 

UPDATE [Contacts] SET
    RobberID = 1 WHERE Contact = 'phone *********** call me maybe'

UPDATE [Robber] SET
    SkillLevel = 100500 WHERE Nickname = 'Mei' */



