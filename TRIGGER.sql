USE TestDB
GO

CREATE TRIGGER DateChange
    ON [Robber] AFTER UPDATE AS
	    IF EXISTS (SELECT deleted.RobberID FROM deleted JOIN inserted ON deleted.RobberID = inserted.RobberID AND deleted.Status != inserted.Status) 
		    UPDATE [Robber] SET
			    [Robber].Date = GETDATE() WHERE RobberID IN (SELECT deleted.RobberID FROM deleted JOIN inserted ON deleted.RobberID = inserted.RobberID AND deleted.Status != inserted.Status)


UPDATE [Robber] SET
   Status = 'ready' WHERE RobberID = 3

SELECT Date FROM [Robber] WHERE RobberID = 3