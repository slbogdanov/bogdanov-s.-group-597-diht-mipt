USE TestDB
GO

/* ��� �� ���� ���� ��� ������ �� ������. ��� ������ ����� �� ������� ���, ��� ��������� � �������. BEGIN TRAN T1 �������� ���������� � ������ �������, BEGIN TRAN 2 - �� ������.
������������������ ������������� ���������*/

/* ������� ������ */

SET TRANSACTION ISOLATION LEVEL READ
COMMITTED

BEGIN TRAN T1

SELECT BankName FROM [Bank]

BEGIN TRAN T2

UPDATE [Bank] SET
    BankName = SUBSTRING(BankName, 1, 2)

BEGIN TRAN T1

SELECT BankName FROM [Bank]

BEGIN TRAN T2

ROLLBACK

BEGIN TRAN T1

SELECT BankName FROM [Bank]

COMMIT

/* �����: ���������� �������� �� ����������, ��� ��� ������� ������ �� �����������*/

/* ��������������� ������ */

SET TRANSACTION ISOLATION LEVEL READ
COMMITTED

BEGIN TRAN T1

SELECT BankName FROM [Bank]

BEGIN TRAN T2

UPDATE [Bank] SET
    BankName = SUBSTRING(BankName, 1, 2)

COMMIT

BEGIN TRAN T1

SELECT BankName FROM [Bank]

COMMIT

/* �����: ��������� ������� ���������, �������������, ��������������� ������ �����������*/