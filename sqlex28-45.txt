28)SELECT CAST(AVG(CAST(SUM AS NUMERIC(6,2))) AS NUMERIC(6,2)) FROM (SELECT SUM(B_VOL) AS SUM FROM (SELECT Q_ID, CASE WHEN B_VOL IS NULL THEN 0 ELSE B_VOL END B_VOL FROM utQ LEFT JOIN utB ON Q_ID = B_Q_ID) X GROUP BY Q_ID) X

29)SELECT CASE WHEN Income_o.point IS NULL THEN Outcome_o.point ELSE Income_o.point END point, CASE WHEN Income_o.date IS NULL THEN Outcome_o.date ELSE Income_o.date END date, inc, out FROM Income_o FULL JOIN Outcome_o ON Income_o.point = Outcome_o.point AND Income_o.date = Outcome_o.date

30)SELECT CASE WHEN point IS NULL THEN p1 ELSE point END point, CASE WHEN date IS NULL THEN d1 ELSE date END date, out ,inc FROM ((SELECT point, date, SUM(inc) AS inc FROM Income GROUP BY point, date) X FULL JOIN (SELECT point AS p1, date AS d1, SUM(out) AS out FROM Outcome GROUP BY point, date) Y ON X.point = Y.p1 AND X.date = Y.d1)

31)SELECT class, country FROM Classes WHERE bore >= 16

32)SELECT DISTINCT country, CAST(AVG(CAST(bore AS NUMERIC(6,2)) * CAST(bore AS NUMERIC(6,2)) * CAST(bore AS NUMERIC(6,2)) / 2) AS NUMERIC(6,2)) as mw FROM (SELECT DISTINCT bore, country, name FROM Classes RIGHT JOIN ((SELECT DISTINCT class AS cls, name FROM Ships INNER JOIN Outcomes ON Outcomes.ship = Ships.name) UNION ALL (SELECT DISTINCT class AS cls, name FROM Ships) UNION ALL (SELECT  DISTINCT class AS cls, ship AS name FROM Classes JOIN Outcomes ON Outcomes.ship = Classes.class)) K ON Classes.class = cls) X GROUP BY country

33)SELECT ship FROM Outcomes WHERE(battle = 'North Atlantic' AND result = 'sunk')

34)SELECT name FROM Ships, Classes WHERE (Ships.class = Classes.class AND Classes.type = 'bb' AND Ships.launched >= 1922 AND Classes.displacement > 35000 AND Ships.launched IS NOT NULL)

35)SELECT model, type FROM Product WHERE model NOT LIKE '%[^0-9^A-Z]%' AND(model NOT LIKE '%[^0-9]%' OR model NOT LIKE '%[^A-Z]%')

36)SELECT DISTINCT Classes.class FROM Classes, Ships, Outcomes WHERE (Outcomes.ship = Classes.class OR Ships.name = Classes.class)

37)SELECT cls FROM (SELECT class AS cls, name FROM Ships UNION SELECT ship AS cls, ship AS name FROM Outcomes JOIN Classes ON ship = class) X GROUP BY cls HAVING count(cls) = 1

38)SELECT country FROM Classes WHERE type = 'bb' INTERSECT SELECT country FROM Classes WHERE type = 'bc'

39)SELECT DISTINCT ship FROM Outcomes AS Out, Battles AS B WHERE result = 'damaged' AND battle = name AND EXISTS (SELECT name FROM Battles JOIN Outcomes ON ship = Out.ship AND battle != Out.battle AND date > B.date AND name = battle)

40)SELECT Ships.class, name, country FROM Ships JOIN Classes ON Ships.class = Classes.class AND numGuns >9

41)SELECT chr, value FROM (SELECT 'cd' AS chr, CAST(cd AS VARCHAR) AS value, code FROM PC UNION SELECT 'hd' AS chr, CAST(hd AS VARCHAR) AS value, code FROM PC UNION SELECT 'price' AS chr, CAST(price AS VARCHAR) AS value, code FROM PC UNION SELECT 'ram' AS chr, CAST(ram AS VARCHAR) AS value, code FROM PC UNION SELECT 'speed' AS chr, CAST(speed AS VARCHAR) AS value, code FROM PC UNION SELECT 'model' AS chr, CAST(model AS VARCHAR) AS value, code FROM PC)X JOIN (SELECT MAX(code) AS code FROM PC)Y ON X.code = Y.code

42)SELECT ship, battle FROM Outcomes WHERE result = 'sunk'

43)SELECT name FROM Battles WHERE DATEPART(year, date) NOT IN (SELECT CASE WHEN launched IS NOT NULL THEN launched ELSE 3000 END launched  FROM Ships)

44)SELECT name FROM Ships WHERE name LIKE 'R%' UNION SELECT ship FROM Outcomes WHERE ship LIKE 'R%'

45)SELECT name FROM Ships WHERE name LIKE '% % %' UNION SELECT ship FROM Outcomes WHERE ship LIKE '% % %'