USE TestDB
GO

IF EXISTS(SELECT * from sys.tables WHERE name='RobberLarge')
BEGIN
DROP TABLE RobberLarge
END
GO
IF EXISTS(SELECT * from sys.tables WHERE name='RobberLargeIndex')
BEGIN
DROP TABLE RobberLargeIndex
END
GO
IF EXISTS(SELECT * from sys.tables WHERE name='BankRobberLarge')
BEGIN
DROP TABLE BankRobberLarge
END
GO
IF EXISTS(SELECT * from sys.tables WHERE name='BankRobberLargeIndex')
BEGIN
DROP TABLE BankRobberLargeIndex
END
GO

CREATE TABLE [dbo].[RobberLarge](
	[Status] [varchar](100) NOT NULL,
	[SkillLevel] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Nickname] [varchar](100) NOT NULL,
	[RobberID] [int] NOT NULL,
	[Spec] [varchar](100) NOT NULL,
);

CREATE TABLE [dbo].[RobberLargeIndex](
	[Status] [varchar](100) NOT NULL,
	[SkillLevel] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Nickname] [varchar](100) NOT NULL,
	[RobberID] [int] NOT NULL,
	[Spec] [varchar](100) NOT NULL,
);

GO
CREATE TABLE [dbo].[BankRobberLarge](
	[RobberID] [int] NOT NULL,
	[BankID] [int] NOT NULL,
	[Share] [int] NOT NULL,
	[OutcomeForRobber] [varchar](100) NOT NULL,
	[ActionsEvaluation] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
);
CREATE TABLE [dbo].[BankRobberLargeIndex](
	[RobberID] [int] NOT NULL,
	[BankID] [int] NOT NULL,
	[Share] [int] NOT NULL,
	[OutcomeForRobber] [varchar](100) NOT NULL,
	[ActionsEvaluation] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
);
GO
INSERT INTO [BankRobberLarge]
    SELECT RobberID, BankID, Share, OutcomeForRobber, ActionsEvaluation, Date FROM BankRobber

INSERT INTO [BankRobberLargeIndex]
    SELECT RobberID, BankID, Share, OutcomeForRobber, ActionsEvaluation, Date FROM BankRobber

INSERT INTO [RobberLarge]
    SELECT Status, SkillLevel, Date, Nickname, RobberID, Spec FROM Robber

INSERT INTO [RobberLargeIndex]
    SELECT Status, SkillLevel, Date, Nickname, RobberID, Spec FROM Robber
GO 100000

SET STATISTICS TIME ON
SET STATISTICS IO ON

CHECKPOINT
GO
DBCC DROPCLEANBUFFERS
GO


CREATE NONCLUSTERED INDEX [IX_RobberNID] ON [dbo].[RobberLargeIndex] (RobberID ASC, Nickname ASC)
CREATE NONCLUSTERED INDEX [IX_BankRobberID] ON [dbo].[BankRobberLargeIndex] (RobberID ASC, BankID ASC)




SELECT RobberID, Nickname FROM RobberLarge WHERE RobberID = 6 AND Nickname = 'Mei'

SELECT RobberID, Nickname FROM RobberLargeIndex WHERE RobberID = 6 AND Nickname = 'Mei'

SELECT Nickname, BankID FROM RobberLarge JOIN BankRobberLarge ON BankRobberLarge.RobberID = RobberLarge.RobberID WHERE RobberLarge.RobberID = 6
GO

SELECT Nickname, BankID FROM RobberLargeIndex JOIN BankRobberLargeIndex ON BankRobberLargeIndex.RobberID = RobberLargeIndex.RobberID WHERE RobberLargeIndex.RobberID = 6
GO

