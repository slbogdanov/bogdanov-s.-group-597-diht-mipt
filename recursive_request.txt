USE master
GO

IF EXISTS(SELECT * from sys.databases WHERE name='TestDB')
BEGIN
    DROP DATABASE TestDB
END

CREATE DATABASE TestDB
GO

USE TestDB
GO

CREATE TABLE TestTable
   (ID int,
    ParentID int)
GO

INSERT TestTable
VALUES (1, NULL),
       (2, 1),
	   (3, 2),
	   (4, 1),
	   (5, 1),
	   (6, 5),
	   (7, 5)
GO

IF OBJECT_ID ('test_view', 'V') IS NOT NULL
DROP VIEW test_view ;
GO

WITH tree (ID, ParentID, Level)
AS
(SELECT ID, ParentID, 0 AS Level 
FROM TestTable
WHERE ID = 3
UNION ALL
SELECT TestTable.ID, TestTable.ParentID, Level + 1
FROM TestTable
INNER JOIN tree ON TestTable.ID = tree.ParentID)
SELECT ID, ParentID
FROM tree
GO