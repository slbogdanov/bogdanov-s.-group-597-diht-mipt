USE TestDB
GO

/* ��� �� ���� ���� ��� ������ �� ������. ��� ������ ����� �� ������� ���, ��� ��������� � �������. BEGIN TRAN T1 �������� ���������� � ������ �������, BEGIN TRAN 2 - �� ������.
������������������ ������������� ���������*/

/* ������� */

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

BEGIN TRAN T1

SELECT * FROM [Bank] WHERE DailyIncome > 4000

BEGIN TRAN T2

SET IDENTITY_INSERT [Bank] ON
INSERT [Bank] ([Address], [SecurityLevel], [BankID], [BankName], [DailyIncome], [Attractiveness]) VALUES ('somewhere100', 6, 14, 'phantom', 9000, 9)
SET IDENTITY_INSERT [Bank] OFF

COMMIT

BEGIN TRAN T1

SELECT * FROM [Bank] WHERE DailyIncome > 4000

COMMIT

/* �����: � ����� �� ���������� ������, �������������, ������� �� ����������� */